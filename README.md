# avr8cs
A simple, tiny (written entirly in C involving inline ASM) Operation System for AVR8 arch which is capable
to run multiple processes in virtually paralel by calling each task after certain period of time if task is not
sleeping/stopped/waiting.
Also OS kernel utilizes custom heap managment library.
The project page available at http://www.nixd.org/en/avr8-projects/avr8-mini-os-kernel
The source code examples can be found at https://www.nixd.org/en/avr8-projects/avr8-mini-os-kernel/code-examples
The source code documentation can be found at http://www.nixd.org/en/avr8-projects/avr8-mini-os-kernel/code-manual

#Demo
The demo will be available on youtube later:

#Recent changes
b20160708
Updated version with the following improvements:
- Added semaphore interlocking
- Added FIFO buffer

