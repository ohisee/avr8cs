#ifndef KERNEL_DEBUG_H_INCLUDED
#define KERNEL_DEBUG_H_INCLUDED
#include "conio.h"
#include "ui.h"
#include "kbd.h"
/*
#if KERNEL_DEBUG == YES
		sp_bk = SP;
		RESTORE_TIMER_STACK();
		kernel_debug(sp_bk, current_kprocess->node->pid, current_kprocess->node->state);
	#endif

	while(1); //wait forever
*/


void kernel_debug(uint8_t * sp_bk);

void MCU_Init(void);

#endif // KERNEL_DEBUG_H_INCLUDED
