/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "kernel_sectioning.h"
#include "kernel_memory_private.h"
#include "kernel_process_private.h"

#if KERNEL_BUILD_WITH_CONDITION_WAIT == YES


ATTRIBUTE_KERNEL_SECTION
struct kproc_wait_condition *
kernel_cond_wait_init(uint8_t * p_cvar, enum simple_condition_eq e_sceq)
{
    if (p_cvar == NULL)
    {
        return NULL;
    }

    //creating structure
    struct kproc_wait_condition * wait = (struct kproc_wait_condition *) kernel_malloc(sizeof(struct kproc_wait_condition));
    if (wait == NULL)
    {
        return RET_FAIL;
    }

    //initializing fields
    wait->cvar = p_cvar;
    wait->e_sceq = e_sceq;

    return wait;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_cond_wait(struct kproc_wait_condition * cond)
{
    if (cond == NULL)
    {
        return RET_FAIL;
    }

    //switch to cooperative mode
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        current_kprocess->node->flags |= KPROC_FLAG_COOP;
    }

    //switching process status to WAIT
    KERNEL_SET_STATE(KSTATE_WAIT)
    {
        //if process is not waiting, then lock, else just leave with error returned
        if (current_kprocess->wait_cond == NULL)
        {
            current_kprocess->wait_cond = cond;
            current_kprocess->node->flags |= KPROC_FLAG_WAIT;

            //releasing control
            kernel_release_control();

            ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
            {
                current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
            }
        }
        else
        {
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
            {
                current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
            }

            return RET_FAIL;
        }
    }

    return RET_OK;
}

#endif // KERNEL_BUILD_WITH_CONDITION_WAIT
