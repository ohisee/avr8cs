/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef KERNEL_CS_H_INCLUDED
#define KERNEL_CS_H_INCLUDED
#include <avr/interrupt.h>
/*
 * Simple Critical sections implementation
 * Critical sections will not be reflected in the performance measurments,
 *  because all intrrupts are disabled during executing non-interruptable code.
 * Please don't put large blocks of codes in one critical section. Split it into,
 *  several and use it during co-operative mode or in normal mode.
 */

void kernel_proc_enter_cooperative_mode();

void kernel_proc_leave_cooperative_mode();

//-----
static __inline__ uint8_t __cs_enter(void)
{
    //kernel_notify_cs_entering(); //ToDo: notify kernel that we are running into critical section
    cli();
    return 1;
}

static __inline__ void __cs_restore(const  uint8_t * sreg)
{
    SREG = *sreg;
    __asm__ volatile ("" ::: "memory");
}

/*
 * the macro to use in code
 */
#define CRITICAL_SECTION()\
    for ( uint8_t sreg __attribute__((__cleanup__(__cs_restore))) = SREG, __ToDo = __cs_enter(); \
	                       __ToDo ; __ToDo = 0 )

/*
 * usage:
 * ...
 * CRITICAL_SECTION()
 * {
 *	owi_write_byte(0xff);
 *	read = owi_read_byte();
 * }
 */
#endif // KERNEL_CS_H_INCLUDED
