/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <avr/wdt.h>
#include "kernel_sectioning.h"
#include "kernel_memory_private.h"
#include "kernel_process_private.h"

#if (KERNEL_TRACK_PROC_STATE == YES)
const char KSTATE_FIFO[] PROGMEM	= "KBFIFO\0"; //kernel basic FIFO
#endif /* KERNEL_TRACK_PROC_STATE */

ATTRIBUTE_KERNEL_SECTION
struct kproc_fifo *
kernel_fifo_init(size_t len)
{
    struct kproc_fifo * kfifo = (struct kproc_fifo *) kernel_malloc(sizeof(struct kproc_fifo));
    if (kfifo == NULL)
    {
        return NULL;
    }

    kfifo->fifo_start = (uint8_t *) kernel_malloc(len);
    if (kfifo->fifo_start == NULL)
    {
        kernel_free((void *) kfifo);
        return NULL;
    }

    kfifo->fifo_end = (size_t)kfifo->fifo_start + (len - 1);
    kfifo->fifo_write = kfifo->fifo_start;
    kfifo->fifo_read = kfifo->fifo_start;

    return kfifo;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_fifo_in(struct kproc_fifo * kfifo, uint8_t data)
{
    KERNEL_SET_STATE(KSTATE_FIFO)
    {
        if (kfifo == NULL)
        {
            return RET_FAIL;
        }
        else if (kernel_fifo_towrite_free_space(kfifo) <= 0)
        {
            return RET_FAIL;
        }

        *(kfifo->fifo_write) = data;

        kfifo->fifo_write++;
        if (kfifo->fifo_write > kfifo->fifo_end)
        {
            kfifo->fifo_write = kfifo->fifo_start;
        }

    }
    return RET_OK;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_fifo_out(struct kproc_fifo * kfifo, uint8_t * data)
{
    KERNEL_SET_STATE(KSTATE_FIFO)
    {
        if ( (kfifo == NULL) || (data == NULL) )
        {
            return RET_FAIL;
        }
        else if (kernel_fifo_toread_space(kfifo) == 0)
        {
            return RET_FAIL;
        }

        (*data) = kfifo->fifo_read;

        kfifo->fifo_read++;
        if (kfifo->fifo_read > kfifo->fifo_end)
        {
            kfifo->fifo_read = kfifo->fifo_start;
        }
    }

    return RET_OK;
}

ATTRIBUTE_KERNEL_SECTION
size_t
kernel_fifo_towrite_free_space(struct kproc_fifo * kfifo)
{
    if (kfifo->fifo_write < kfifo->fifo_read)
    {
        //R - W
        return ( (size_t) (kfifo->fifo_read - kfifo->fifo_write) ) - 1;
    }
    else if (kfifo->fifo_write == kfifo->fifo_read)
    {
        return ( (size_t) (kfifo->fifo_end - kfifo->fifo_start)) - 1;
    }
    else
    {
        //T-W+(T-(T-R)
        uint8_t * buf_len = kfifo->fifo_end - kfifo->fifo_start + 1;
        return ( (size_t) (buf_len - kfifo->fifo_write) + ( buf_len - ( buf_len - kfifo->fifo_read )) ) - 1;
    }
}

ATTRIBUTE_KERNEL_SECTION
size_t
kernel_fifo_total_space(struct kproc_fifo * kfifo)
{
    return (size_t) (kfifo->fifo_end - kfifo->fifo_start + 1);
}

ATTRIBUTE_KERNEL_SECTION
size_t
kernel_fifo_toread_space(struct kproc_fifo * kfifo)
{
    return ( (size_t) (kfifo->fifo_end - kfifo->fifo_start)) - kernel_fifo_towrite_free_space(kfifo);
}
