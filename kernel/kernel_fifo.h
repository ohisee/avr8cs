/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef KERNEL_QUEUE_H_INCLUDED
#define KERNEL_QUEUE_H_INCLUDED

/*
 * struct kproc_fifo
 * see kernel_process_private.h
 */
struct kproc_fifo;

/*
 * kernel_fifo_init
 * FIFO buffer dynamic initialization.
 * len (size_t) the length of the fifo buffer
 * returs pointer to the FIFO buffer descriptor or NULL
 */
struct kproc_fifo * kernel_fifo_init(size_t len);

/*
 * kernel_fifo_in
 * This function pushes byte to fifi buffer
 * kq (struct kproc_fifo*) pointer to valid fifo descriptor
 * data (uint8_t) data to push
 * returns: return 0 on success or 1 on error
 */
int8_t kernel_fifo_in(struct kproc_fifo * kfifo, uint8_t data);

/*
 * kernel_fifo_out
 * This function retrives first element.
 * kq (struct kproc_fifo*) pointer to valid fifo descriptor
 * data (uint8_t) retrived value to store
 * returns: return 0 on success or 1 on error
 */
int8_t kernel_fifo_out(struct kproc_fifo * kfifo, uint8_t * data);

/*
 * kernel_fifo_towrite_free_space
 * This function returns free space left to write in FIFO buffer.
 * kq (struct kproc_fifo*) pointer to valid fifo descriptor
 * returns: returns 0 if no space left or left space value
 */
size_t kernel_fifo_towrite_free_space(struct kproc_fifo * kfifo);

/*
 * kernel_fifo_total_space
 * This function returns total size of the FIFO buffer.
 * kq (struct kproc_fifo*) pointer to valid fifo descriptor
 * returns: returns total value
 */
size_t kernel_fifo_total_space(struct kproc_fifo * kfifo);

/*
 * kernel_fifo_toread_space
 * This function returns the amount to read left.
 * kq (struct kproc_fifo*) pointer to valid fifo descriptor
 * returns: 0 if nothing to read or the amount to read
 */
size_t kernel_fifo_toread_space(struct kproc_fifo * kfifo);

#endif // KERNEL_QUEUE_H_INCLUDED
