/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

 /*
  * The max addressable RAM: 16383Bytes ~ 16kB
  *  i.e from 0x200 up to 0x3FFF
  */


#ifndef KERNEL_MEMORY_PRIVATE_H_
#define KERNEL_MEMORY_PRIVATE_H_

#include "kernel_memory.h"

#pragma pack()

/*
 * enum kern_merge_type
 * defines the merging operation order P<-C or C<-N
 */
enum kern_merge_type
{
	K_MERGE_CURRENT_PREVIOUS    = (uint8_t) 1,
	K_MERGE_CURRENT_NEXT        = (uint8_t) 2,
};

/*
 * struct memblock
 * general memory block structure
 */
struct memblock
{
	block_size_t addr : 14;
	block_size_t mask : 2;
	uint8_t heap[];
};

extern volatile uint8_t * heap_start;                   //start of the heap
extern volatile uint8_t * heap_end;                     //end of the heap
extern volatile block_size_t total_mem_avail;           //total heap RAM
extern volatile block_size_t total_mem_used;            //total heap used
extern volatile block_size_t total_mem_descr;           //total mem descriptors
extern volatile block_size_t total_mem_frag;            //total mem fragmentation
extern volatile struct memblock * volatile memblocks;   //memoryblock ptr

//mutex
extern volatile struct kproc_mutex * volatile memmux;   //alocation mutex

#define GET_BLOCK_SIZE(__BLOCK__) ((block_size_t) __BLOCK__->addr - (block_size_t) __BLOCK__)
#define GET_BLOCK_SIZE_WITHOUT_HEAD(__BLOCK__)  ((block_size_t) __BLOCK__->addr - (block_size_t) __BLOCK__) - sizeof(struct memblock)
#define GET_NEXT_BLOCK(__BLOCK__) (struct memblock *) ((block_size_t)__BLOCK__->addr)

/*
 * __kern_resize_block
 * Resizes selected block to new size by modifying block address field.
 * block (struct memblock *) - pointer to the header of the selected block
 * newsize (size_t) - new size of the block
 * returns: nothing
 */
void __kern_resize_block(struct memblock * block, size_t newsize);

/*
 * __mem_allocate
 *  allocates the whole new chunk of memory or reuse previousl used.
 *	if function allocates new chunk the total occupied memory will be allocs+sizeof(struct memblock)
 *	block (struct memblock *) valid pointer to the allocation
 *  allocs (site_t) the amount to allocate
 *	returns pointer to the start of the allocated heap or NULL
 */
void * __mem_allocate(struct memblock * block, size_t allocs);

/*
 * __mem_try_allocate
 *  function is called when the deallocated block was found but it is larger than
 *		required length.
 *	prev_nearest_block (struct memblock * ) the block located before nearest_block
 *	nearest_block (struct memblock * ) found block
 *	allocs (size_t) the amount to allocate
 *	return pointer to the start of the allocated heap or NULL
 */
void * __mem_try_allocate(struct memblock * prev_nearest_block, struct memblock * nearest_block, size_t allocs);

/*
 * __kern_resize_block
 * procedure is called when it is nessesary to resie the existing deallocated block.
 * block (struct memblock *) the block to resize
 * newsize (sizte_t) new size of the resized block
 */
void __kern_resize_block(struct memblock * block, size_t newsize);

/*
 * __kern_reduce_block
 * function is called when the new block is placed in the previously used block and require to
 *	reduce its size
 * blkc (struct memblock *) current block to crete new header
 * newsize (size_t) new size
 * returns pointer to the created block which was generated from created
 */
struct memblock * __kern_reduce_block(struct memblock * blkc, size_t newsize);

/*
 * __kern_merge_blocks
 * This function performes resize to larger blob by merging blocks
 *  (previous with current or current with next)
 *  depending on the merge_type selector with block space reallocation and data
 *  moving from one space to another.
 *  K_MERGE_CURRENT_PREVIOUS:
 *      the current will be merged to previous and memory will be moved from
 *      current block to previous
 *  K_MERGE_CURRENT_NEXT:
 *      the next block(blk_pn) will be merged to current and new block will be created if
 *      free space was left
 *  e_merge_type (enum kern_merge_type) - see enum kern_merge_type at the top of the file
 *  blk_c (struct memblock *) - pointer to the head of the current block
 *  blk_cs (size_t) - size of the current block
 *  newsize (size_t) - new size (desired block size)
 *  blk_pn (struct memblock *) - pointer to the head of the previous/next block
 *  blk_pns (size_t) - size of the prev/next block
 *  returns: pointer to the head of the block of the left unused or fragmented space
 */
struct memblock *
__kern_merge_blocks(enum kern_merge_type e_merge_type,
					struct memblock * blk_c,
					size_t blk_cs,
					size_t newsize,
					struct memblock * blk_pn,
					size_t blk_pns);

/*
 * __kern_use_near_block
 * This function is called during reallocation when it was decided to
 *  reuse the previously used/
 *  deallocated block which is likely to be considered as nearest, because
 *  no other better space was found, but it is larger than new block.
 * block_near (struct memblock *) - the selected (nearest) block
 * block_near_len (block_size_t) - the size of the nearest block
 * resize (size_t) - new size of the block
 * ptr_block (struct memblock *) - pointer to the block that is currently reallocating (old)
 * ptr_block_len (block_size_t) - the size of the old block
 * returns: pointer the start of the heap of the block_near argument
 */
void *
__kern_use_near_block(struct memblock * block_near,
                        block_size_t block_near_len,
						size_t resize,
						struct memblock * ptr_block,
						block_size_t ptr_block_len);

/*
 * __kern_defragment
 * This function merges previous block with current block.
 *  The following conditions should be met:
 *      - block_prev != NULL
 *      - block_prev != (*block)
 *      - (block_prev->mask & MEM_FLAG_BUSY) == 0
 * block_prev (struct memblock *) - pointer to previous block
 * block (struct memblock **) - current block p2p which will be left modified
 * returns: 0 on success or 1 on error
 */
int8_t __kern_defragment(struct memblock * block_prev, struct memblock ** block);


#endif /* KERNEL_MEMORY_PRIVATE_H_ */
