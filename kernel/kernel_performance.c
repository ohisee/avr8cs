/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "kernel_sectioning.h"
#include "kernel_process_private.h"

#if (KERNEL_MEASURE_PERF==YES)

volatile uint8_t cpu_utiliz_total		= 0;

#if (KERNEL_MEASURE_PERF_ISR == YES)

volatile uint8_t timer1_ovfs            = 0;
volatile uint32_t isr_tcnt_cnt          = 0;
volatile uint32_t isr_tcnt_counted      = 0;
#endif

ATTRIBUTE_KERNEL_SECTION
uint8_t
kernel_get_proc_cpu_utiliz()
{
	return cpu_utiliz_total;
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_kernel_process_count_time_spent()
{
	current_kprocess->node->tm_cc += abs(OCR0A - current_kprocess->node->tcnt0_start);
	return;
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_kernel_process_start_measure_time()
{
	current_kprocess->node->tcnt0_start = TCNT0;
	return;
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_kernel_cpu_utilization_eval()
{
	cpu_utiliz_total = 0;
#if (KERNEL_MEASURE_PERF_ISR == YES)
	isr_tcnt_counted = isr_tcnt_cnt;//(uint16_t) (div(isr_millis_s , 10).quot);
	isr_tcnt_cnt = 0;
#endif
	volatile struct kprocess_list * volatile temp = kproc_list;
	while (temp != NULL)
	{
		temp->node->cpu_usage = (uint8_t) (div((uint32_t)(temp->node->tm_cc * 0.004f), 10).quot);
		cpu_utiliz_total += temp->node->cpu_usage;
		temp->node->tm_cc = 0;
		temp = temp->next;
	}
}

#if (KERNEL_MEASURE_PERF_ISR == YES)


ISR(TIMER1_OVF_vect)
{
    ++timer1_ovfs;
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_bind_timer1()
{
    //timer 1
	OCR1A = 0;
	OCR1B = 0;
	OCR1C = 0;
	//allow overflow intrrupt
	TIMSK1 = (1 << TOV1);
	//Normal mode + 1024 prescaller should be selected
	TCCR1A = 0;

	return;
}

ATTRIBUTE_KERNEL_SECTION
uint32_t
kernel_get_isr_cpu_msec()
{
	return (isr_tcnt_counted * 0.064);
}

ATTRIBUTE_KERNEL_SECTION
uint64_t
kernel_get_isr_cpu_usec()
{
	return (isr_tcnt_counted * 64);
}

ATTRIBUTE_KERNEL_SECTION
uint32_t
kernel_isr_tcnt2ms(uint32_t tcnt)
{
	return (tcnt * 0.064);
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_kern_measure_isr()
{
    timer1_ovfs = 0;
    TCNT1 = 0;
    TCCR1B = (1 << CS12) | (1 << CS10);
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_kern_count_isr(struct kisr * kisrv)
{
    TCCR1B = 0;

    uint32_t tcnt_cnt = TCNT1 + (timer1_ovfs * 65535);

    isr_tcnt_cnt += tcnt_cnt;
    kisrv->tcnt_cnt += tcnt_cnt;

    kisrv->total_cals += 1;


	return;
}
#endif // (KERNEL_MEASURE_PERF_ISR == YES)
#endif // (KERNEL_MEASURE_PERF==YES)

