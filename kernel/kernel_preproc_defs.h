/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef KERNEL_PREPROC_DEFS_H_
#define KERNEL_PREPROC_DEFS_H_

#define VERSION 20170121
#define F_CPU 16000000UL
#define F_OSC F_CPU
//#define HEAP_ENDS_AT 4600
//YES, NO definition
#define YES 1
#define NO 0

//---return value standart
#define RET_OK 0
#define RET_FAIL 1
#define RET_NOID -1

#define MAX_ERROR_SIZE 10

#define MCUSR_EXTERNAL YES

//*******MEMORY
/*
 * MEM_USE_UNSAFE_FREE removes code which performs checks.
 *  Enabled - faster deallocation, Disabled - slover deallocation,
 *      but safer
 */
#define KERNEL_MEM_USE_UNSAFE_FREE YES

/*
 * MEM_CONT_RAM perform tracking of used memeory, descriptors, etc
 *	Mostly used to track performance
 */
#define KERNEL_MEM_CONT_RAM YES

/*
 * CONDITION_WAIT is a feature which allows to set process to WAIT state
 *  until the external event triggers the wake-up of the process.
 */
#define KERNEL_BUILD_WITH_CONDITION_WAIT YES

/*
 * BUILD_WITH_ISR - build kernel with ISR handling.
 */
#define KERNEL_BUILD_WITH_ISR YES

/*
 * PROC_ISR_STATCK_SIZE the stack size of the ISR_ITERATOR process.
 */
#define KERNEL_PROC_ISR_STATCK_SIZE 250

/*
 * MEASURE_PERF
 * Build with performance monitor or without it for both ISR and processes/
 * TRACK_PROC_STATE
 * Build with process state tracking - the current status.
 * By disabling this sections, improves CPU and memory performance
 */
#define KERNEL_MEASURE_PERF     YES
#define KERNEL_MEASURE_PERF_ISR YES
#define KERNEL_TRACK_PROC_STATE YES

/*
 * USE_PROCESS_TITLES, USE_ISR_TITLES
 * If it is necessary to keep process and ISR titles then enable either or both.
 * For basic functionality it is not necessary.
 * By disabling this sections, improves CPU and memory performance
 */
#define KERNEL_USE_PROCESS_TITLES   YES
#define KERNEL_USE_ISR_TITLES       YES

//DEBUG
#define KERNEL_DEBUG            NO //remove

#define WATCHDOG_MCURS_HANDLER_BUILDIN YES

/*
 * BUILD_WITH_ISR
 * Automitic options control for ISR
 */
#if KERNEL_BUILD_WITH_ISR == NO
    #define KERNEL_MEASURE_PERF_ISR NO
    #define KERNEL_USE_ISR_TITLES   NO
#endif // KERNEL_BUILD_WITH_ISR

#endif /* KERNEL_PREPROC_DEFS_H_ */
