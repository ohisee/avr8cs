/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef KERNEL_SECTIONING_H_
#define KERNEL_SECTIONING_H_

#define STR(x) #x
#define KERNEL_SECTION	.text.kernel
#define ATTRIBUTE_KERNEL_SECTION __attribute__ ((section (STR(KERNEL_SECTION))))
#define ASM_KERNEL_SECTION .section KERNEL_SECTION, "ax", @progbits


#endif /* KERNEL_SECTIONING_H_ */
