/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef KAVR_IO_H_INCLUDED
#define KAVR_IO_H_INCLUDED

/*
 * This section of the project requires porting to other AVR8 MCUs
 *  because of the difference in the interrupt vector numbers.
 */

/* avr/iomxx0_1.h - definitions for ATmega640, Atmega1280, ATmega1281,
   ATmega2560 and ATmega2561.
    see iomxx0_1.h in include/avr folder
 */
#if defined (__AVR_ATmega2560__) || defined(__AVR_ATmega2561__)\
    || (__AVR_ATmega640__) || defined(__AVR_ATmega1280__)\
    || (__AVR_ATmega1281__)
	#include "kiomxx0_1.h"
#else

	#if !defined(__COMPILING_AVR_LIBC__)

		#error "device type not defined"

	#endif

#endif // defined


#endif // KERNEL_ISR_H_INCLUDED
