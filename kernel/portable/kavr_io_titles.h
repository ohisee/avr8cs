#ifndef KAVR_IO_TITLES_H_INCLUDED
#define KAVR_IO_TITLES_H_INCLUDED
/*
 * This section of the project requires porting to other AVR8 MCUs
 *  because of the difference in the interrupt handlers.
 */

#include <avr/pgmspace.h>
#include <stdint.h>
#include <stdio.h>

/* avr/iomxx0_1.h - definitions for ATmega640, Atmega1280, ATmega1281,
   ATmega2560 and ATmega2561.
    see iomxx0_1.h in include/avr folder
 */
#if defined (__AVR_ATmega2560__)
	#include "kiomxx0_1_titles.h"
#else

	#if !defined(__COMPILING_AVR_LIBC__)

		#error "device type not defined"

	#endif

#endif // defined

#endif // KAVR_IO_TITLES_H_INCLUDED
