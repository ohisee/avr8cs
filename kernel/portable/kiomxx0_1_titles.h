#ifndef KIOMXX0_1_TITLES_H_INCLUDED
#define KIOMXX0_1_TITLES_H_INCLUDED
#include <avr/pgmspace.h>
//#if KERNEL_USE_ISR_TITLES == YES
/*
 * ISR Captions table
 */
const char vec01[] PROGMEM = "SIG_INTERRUPT\0";
const char vec02[] PROGMEM = "SIG_INTERRUPT1\0";
const char vec03[] PROGMEM = "SIG_INTERRUPT2\0";
const char vec04[] PROGMEM = "SIG_INTERRUPT3\0";
const char vec05[] PROGMEM = "SIG_INTERRUPT4\0";
const char vec06[] PROGMEM = "SIG_INTERRUPT5\0";
const char vec07[] PROGMEM = "SIG_INTERRUPT6\0";
const char vec08[] PROGMEM = "SIG_INTERRUPT7\0";
const char vec09[] PROGMEM = "SIG_PIN_CHANGE0\0";
const char vec10[] PROGMEM = "SIG_PIN_CHANGE1\0";
const char vec11[] PROGMEM = "SIG_PIN_CHANGE2\0";
const char vec12[] PROGMEM = "SIG_WATCHDOG_TIMEOUT\0";
const char vec13[] PROGMEM = "SIG_OUTPUT_COMPARE2A\0";
const char vec14[] PROGMEM = "SIG_OUTPUT_COMPARE2B\0";
const char vec15[] PROGMEM = "SIG_OVERFLOW2\0";
const char vec16[] PROGMEM = "SIG_INPUT_CAPTURE1\0";
const char vec17[] PROGMEM = "SIG_OUTPUT_COMPARE1A\0";
const char vec18[] PROGMEM = "SIG_OUTPUT_COMPARE1B\0";
const char vec19[] PROGMEM = "SIG_OUTPUT_COMPARE1C\0";
const char vec20[] PROGMEM = "SIG_OVERFLOW1\0";
const char vec21[] PROGMEM = "SIG_OUTPUT_COMPARE0A\0";
const char vec22[] PROGMEM = "SIG_OUTPUT_COMPARE0B\0";
const char vec23[] PROGMEM = "SIG_OVERFLOW0\0";
const char vec24[] PROGMEM = "SIG_SPI\0";
const char vec25[] PROGMEM = "SIG_USART0_RECV\0";
const char vec26[] PROGMEM = "SIG_USART0_DATA\0";
const char vec27[] PROGMEM = "SIG_USART0_TRANS\0";
const char vec28[] PROGMEM = "SIG_COMPARATOR\0";
const char vec29[] PROGMEM = "SIG_ADC\0";
const char vec30[] PROGMEM = "SIG_EEPROM_READY\0";
const char vec31[] PROGMEM = "SIG_INPUT_CAPTURE3\0";
const char vec32[] PROGMEM = "SIG_OUTPUT_COMPARE3A\0";
const char vec33[] PROGMEM = "SIG_OUTPUT_COMPARE3B\0";
const char vec34[] PROGMEM = "SIG_OUTPUT_COMPARE3C\0";
const char vec35[] PROGMEM = "SIG_OVERFLOW3\0";
const char vec36[] PROGMEM = "SIG_USART1_RECV\0";
const char vec37[] PROGMEM = "SIG_USART1_DATA\0";
const char vec38[] PROGMEM = "SIG_USART1_TRANS\0";
const char vec39[] PROGMEM = "SIG_2WIRE_SERIAL\0";
const char vec40[] PROGMEM = "SIG_SPM_READY\0";
const char vec41[] PROGMEM = "SIG_INPUT_CAPTURE4\0";
const char vec42[] PROGMEM = "SIG_OUTPUT_COMPARE4A\0";
const char vec43[] PROGMEM = "SIG_OUTPUT_COMPARE4B\0";
const char vec44[] PROGMEM = "SIG_OUTPUT_COMPARE4C\0";
const char vec45[] PROGMEM = "SIG_OVERFLOW4\0";
const char vec46[] PROGMEM = "SIG_INPUT_CAPTURE5\0";
const char vec47[] PROGMEM = "SIG_OUTPUT_COMPARE5A\0";
const char vec48[] PROGMEM = "SIG_OUTPUT_COMPARE5B\0";
const char vec49[] PROGMEM = "SIG_OUTPUT_COMPARE5C\0";
const char vec50[] PROGMEM = "SIG_OVERFLOW5\0";
const char vec51[] PROGMEM = "SIG_USART2_RECV\0";
const char vec52[] PROGMEM = "SIG_USART2_DATA\0";
const char vec53[] PROGMEM = "SIG_USART2_TRANS\0";
const char vec54[] PROGMEM = "SIG_USART3_RECV\0";
const char vec55[] PROGMEM = "SIG_USART3_DATA\0";
const char vec56[] PROGMEM = "SIG_USART3_TRANS\0";
const char vec57[] PROGMEM = "SIG_OUTPUT_COMPARE5A\0";
const char vec58[] PROGMEM = "SIG_OUTPUT_COMPARE5B\0";
const char vec59[] PROGMEM = "SIG_OUTPUT_COMPARE5C\0";
const char vec60[] PROGMEM = "SIG_OVERFLOW5\0";

const char * const isrcaps0[60] PROGMEM =
{
  vec01,
  vec02,
  vec03,
  vec04,
  vec05,
  vec06,
  vec07,
  vec08,
  vec09,
  vec10,
  vec11,
  vec12,
  vec13,
  vec14,
  vec15,
  vec16,
  vec17,
  vec18,
  vec19,
  vec20,
  vec21,
  vec22,
  vec23,
  vec24,
  vec25,
  vec26,
  vec27,
  vec28,
  vec29,
  vec30,
  vec31,
  vec32,
  vec33,
  vec34,
  vec35,
  vec36,
  vec37,
  vec38,
  vec39,
  vec40,
  vec41,
  vec42,
  vec43,
  vec44,
  vec45,
  vec46,
  vec47,
  vec48,
  vec49,
  vec50,
  vec51,
  vec52,
  vec53,
  vec54,
  vec55,
  vec56,
  vec57,
  vec58,
  vec59,
  vec60
};
//#endif // KERNEL_USE_ISR_TITLES


#endif // KIOMXX0_1_TITLES_H_INCLUDED
