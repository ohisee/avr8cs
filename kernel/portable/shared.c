/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "../kernel_sectioning.h"
#include "../kernel_process_private.h"
#include "shared.h"

ATTRIBUTE_KERNEL_SECTION
void
default_isr_code_section(uint8_t _vec_num_, struct kisr ** isr_ptr)
{
    //run time measurment
    _ISR_MEASURE_E();

    //if cache of ISR is empty then try to retreive it
    if ((*isr_ptr) == NULL)
    {
        //get record
        (*isr_ptr) = kernel_isr_get(_vec_num_);

        //if record was not found then leave
        if ((*isr_ptr) == NULL)
        {
            goto exit_error;
        }
    }
    else if ((*isr_ptr)->flags.flags & KISRF_SUSPEND)
    {
        //ISR Is suspended, leave
        goto exit_error;
    }
    else if ((*isr_ptr)->flags.flags & KISRF_DELAYED)
    {
        //Task is delayed, set flag
        (*isr_ptr)->flags.cnt++; //|= KISRF_ISR;
        //increase counter
        ++isr_to_process;
    }
    else
    {
        //Call handler
        (*isr_ptr)->ISR_HANDLER();
    }
//leaving the ISR default routine
exit_error:
    //call timer to stop measure and save recorded time
    _ISR_MEASURE_L((*isr_ptr));
}

